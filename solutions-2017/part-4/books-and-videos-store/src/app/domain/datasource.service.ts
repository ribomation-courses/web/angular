import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/empty';

import {environment} from '../../environments/environment';
import {Book, Video} from './products';

@Injectable()
export class DataSourceService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.backEnd.url;
  }

  get books(): Observable<Book[]> {
    // return this.http
    //   .get(`${this.url}/books`)
    //   .map(res => res.json())
    //   .do(json => console.log('[ds]', 'books', json))
    //   ;

    return this.http.get<Book[]>(`${this.url}/books`);
  }

  get publishers(): Observable<string[]> {
    return this.books
      .map(books => books.map(book => book.publisher))
      .map(publishers => publishers.sort())
      .map(publishers => publishers.filter((pub, idx, arr) => idx == arr.indexOf(pub)))
      ;
  }

  searchBook(phrase: string): Observable<Book[]> {
    if (!phrase || phrase.trim().length ===0) {
      return Observable.empty();
    }

    return this.books
      .map(books => books.filter((book) => {
        const txt = (book.title + book.authors.join('')).toLowerCase();
        return txt.includes(phrase.toLowerCase());
      }))
      ;
  }

}
