import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';

import {DataSourceService} from '../../domain/datasource.service';
import {Book} from '../../domain/products';
import {ShoppingCartService} from '../../purchase/shopping-cart.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  // books: Observable<Book[]>;
  books: Book[];
  publishers: Observable<string[]>;
  selectedPublisher: string = null;

  phrase: FormControl = new FormControl();
  savedBooks: Book[] = null;

  pagination: any = {
    currentPage: 1,
    booksCount: 1,
    booksPerPage: 3,
    visible: (idx: number): boolean => {
      if (this.selectedPublisher) return false;
      const N = this.pagination.booksPerPage;
      const IDX = (this.pagination.currentPage - 1) * N;
      return (IDX <= idx) && (idx < (IDX + N));
    }
  };

  constructor(private ds: DataSourceService,
              private cart: ShoppingCartService) {
  }

  ngOnInit() {
    this.publishers = this.ds.publishers;

    this.ds.books
      .subscribe(items => {
        this.books = items;
        this.pagination.booksCount = items.length;
      })
    ;

    this.phrase
      .valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(txt => this.ds.searchBook(txt))
      .do(res => console.log('[search]', res))
      .subscribe((results: Book[]) => {
        if (this.phrase.value.length > 0 || results.length > 0) {
          if (!this.savedBooks) this.savedBooks = this.books;
          this.books = results;
        } else {
          this.books = this.savedBooks || [];
          this.savedBooks = null;
        }

        console.log('[search]', 'phrase', this.phrase.value);
        console.log('[search]', 'results.len', results.length);
        console.log('[search]', 'books.len', this.books.length);
        console.log('[search]', 'saved.len', (this.savedBooks || []).length);
      })
    ;
  }

  clearSearch(): void {
    this.phrase.reset();
    this.books = this.savedBooks || [];
    this.savedBooks = null;
  }

  selectPublisher(pub: string): void {
    console.log('[books]', 'select', pub);
    this.selectedPublisher = pub !== 'all' ? pub : null;
  }


  add(book: Book): void {
    console.log('[books]', 'add', book);
    this.cart.add(book);
  }

}
