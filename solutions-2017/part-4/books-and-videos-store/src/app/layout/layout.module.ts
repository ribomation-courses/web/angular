import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from "./header/header.component";
import {SidebarComponent} from "./sidebar/sidebar.component";
import {FooterComponent} from "./footer/footer.component";
import {UtilsModule} from "../utils/utils.module";
import {PurchaseModule} from '../purchase/purchase.module';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    UtilsModule,
    PurchaseModule,
    RouterModule,
  ],
  declarations: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent
  ]
})
export class LayoutModule { }
