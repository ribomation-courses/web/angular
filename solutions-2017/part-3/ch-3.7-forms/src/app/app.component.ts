import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  signup: FormGroup;

  constructor(private bld: FormBuilder) {
    this.setup();
  }

  setup(): void {
    this.signup = this.bld.group({
        name: ['', [
          Validators.required,
          Validators.minLength(3)]],
        email: ['', [
          Validators.required,
          Validators.email
        ]],
        address: ['', [
          Validators.required,
          Validators.minLength(5)
        ]],
      }
    );
  }

  submit(): void {
    if (this.signup.valid) {
      console.info('form-data', this.signup.value);
    }else {
      console.warn('form invalid',
        this.signup.status,
        this.signup.value);
    }
  }

}
