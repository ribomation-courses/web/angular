import {Inject, Injectable} from '@angular/core';
import {FACTOR} from './tokens';

@Injectable()
export class MultiplierService {

  constructor(@Inject(FACTOR) private factor: number) {
  }

  multiply(arg: number): number {
    return arg * this.factor;
  }

}
