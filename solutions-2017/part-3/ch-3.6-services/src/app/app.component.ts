import {Component} from '@angular/core';
import {MultiplierService} from './multiplier.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  arg: number = 10;
  result: number = 0;

  constructor(private svc: MultiplierService) {
    this.compute();
  }

  compute(): void {
    this.result = this.svc.multiply(this.arg);
  }
}
