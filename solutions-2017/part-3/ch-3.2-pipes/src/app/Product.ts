export class Product {
  name: string = '';
  price: number = 0;
  isService: boolean = false;
  itemsInStore: number = 0;
  lastUpdated: Date = new Date();
}

function randInt(lb: number, ub: number): number {
  return Math.floor(lb + (ub - lb + 1) * Math.random());
}

const names: string[] = [
  'Angular Super Book', 'TypeScript Ultra Book', 'JavaScript for Hackers',
  'NodeJS Ultimate Bible', 'REST Web Services with ExpressJS'
];

export class Generator {
  static mk(): Product {
    let p: Product = new Product();
    p.name = names[randInt(0, names.length - 1)];
    p.itemsInStore = randInt(0, 10);
    p.price = randInt(100, 500);
    p.lastUpdated = new Date(Date.now() - randInt(1, 30) * 24 * 3600 * 1000);
    p.isService = Math.random() < 0.5;
    return p;
  }

  static generate(n: number): Product[] {
    let lst: Product[] = [];
    for (let k = 0; k < n; ++k) lst.push(this.mk());
    return lst;
  }

}
