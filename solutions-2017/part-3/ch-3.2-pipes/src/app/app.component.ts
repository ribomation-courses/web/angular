import { Component } from '@angular/core';
import {Product,Generator} from './Product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Pipes';
  products:Product[];

  constructor() {
    this.products = Generator.generate(10);
  }

}
