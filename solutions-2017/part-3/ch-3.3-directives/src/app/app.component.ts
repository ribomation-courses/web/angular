import {Component} from '@angular/core';
import {Vehicle, Generator} from './vehicle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  count: number = 5;
  vehicles: Vehicle[] = [];

  constructor() {
    this.update();
  }

  update(): void {
    this.vehicles = Generator.generate(this.count);
  }


}
