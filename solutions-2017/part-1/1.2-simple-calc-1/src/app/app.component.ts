import {Component, OnInit} from "@angular/core";
import {Result} from "./result";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  lhs: number = 40;
  rhs: number = 2;
  results: Result[] = [];

  constructor() {
  }

  ngOnInit(): void {
    this.compute();
  }

  compute(): void {
    this.results = [];
    this.results.push(AppComponent.add(this.lhs, this.rhs));
    this.results.push(AppComponent.sub(this.lhs, this.rhs));
    this.results.push(AppComponent.mul(this.lhs, this.rhs));
    this.results.push(AppComponent.div(this.lhs, this.rhs));
  }

  static add(lhs: number, rhs: number): Result {
    return new Result("Addition", lhs, "+", rhs, lhs + rhs);
  }

  static mul(lhs: number, rhs: number): Result {
    return new Result("Multiplication", lhs, "*", rhs, lhs * rhs);
  }

  static sub(lhs: number, rhs: number): Result {
    return new Result("Subtraction", lhs, "-", rhs, lhs - rhs);
  }

  static div(lhs: number, rhs: number): Result {
    return new Result("Division", lhs, "/", rhs, lhs / rhs);
  }

}
