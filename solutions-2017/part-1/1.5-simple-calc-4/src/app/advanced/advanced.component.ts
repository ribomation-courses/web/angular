/*
import { Component, OnInit } from '@angular/core';
import {Result} from "../result";

@Component({
  selector: 'app-advanced',
  templateUrl: './advanced.component.html',
  styleUrls: ['./advanced.component.css']
})
export class AdvancedComponent implements OnInit {
  results: Result[] = [];
  constructor() { }
  ngOnInit() {}
}
*/
import { Component, OnInit } from '@angular/core';
import {Result} from "../result";
import {OperandsService} from "../services/operands.service";
import {HttpEvalService} from "../services/http-eval.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/do";

@Component({
  selector: 'app-results-advanced',
  templateUrl: './advanced.component.html',
  styleUrls: ['./advanced.component.css']
})
export class AdvancedComponent implements OnInit {
  results: Observable<Result[]>;
  loading: boolean = false;

  constructor(private httpEvalSvc: HttpEvalService,
              private opsSvc: OperandsService) {
  }

  ngOnInit() {
    this.results = this.opsSvc.feed()
      .do(() => {
        this.loading = true;
      })
      .switchMap(v => this.httpEvalSvc.computeFeed("adv", v.lhs))
      .do(() => {
        this.loading = false;
      })
    ;
  }
}
