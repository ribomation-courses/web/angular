import {Routes} from "@angular/router";
import {ResultsComponent} from "./results/results.component";
import {AdvancedComponent} from "./advanced/advanced.component";
import {NotFoundComponent} from "./not-found/not-found.component";

export const appRoutes: Routes = [
  {path: "simple", component: ResultsComponent},
  {path: "advanced", component: AdvancedComponent},
  {path: "", redirectTo: "/simple", pathMatch: "full"},
  {path: "**", component: NotFoundComponent}
];

