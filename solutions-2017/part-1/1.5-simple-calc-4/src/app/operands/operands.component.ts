/*
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-operands',
  templateUrl: './operands.component.html',
  styleUrls: ['./operands.component.css']
})
export class OperandsComponent implements OnInit {
  lhs: number = 40;
  rhs: number = 2;
  @Output('left') lhsOut: EventEmitter<number>;
  @Output('right') rhsOut: EventEmitter<number>;

  constructor() {
    this.lhsOut = new EventEmitter<number>();
    this.rhsOut = new EventEmitter<number>();
  }

  compute():void {
    console.info('[operands]', 'compute()', this.lhs, this.rhs);
    this.lhsOut.emit(this.lhs);
    this.rhsOut.emit(this.rhs);
  }

  ngOnInit() {}
}
*/
import {Component, Input, OnInit} from "@angular/core";
import {FormControl} from "@angular/forms";
import {OperandsService} from "../services/operands.service";
import 'rxjs/add/operator/filter';

@Component({
  selector: "app-operands",
  templateUrl: "./operands.component.html",
  styleUrls: ["./operands.component.css"]
})
export class OperandsComponent implements OnInit {
  @Input("hideRight") hideRight: boolean = false;
  lhsField: FormControl;
  rhsField: FormControl;

  constructor(private opsSvc: OperandsService) {
    this.lhsField = new FormControl();
    this.rhsField = new FormControl();
  }

  ngOnInit() {
    this.opsSvc.setSources(this.lhsField, this.rhsField);
    setTimeout(() => {
      this.lhsField.setValue(40);
      this.rhsField.setValue(2);
    }, 1000);
  }

  compute(): void {
    this.lhsField.setValue(this.lhsField.value);
    this.rhsField.setValue(this.rhsField.value);
  }
}
