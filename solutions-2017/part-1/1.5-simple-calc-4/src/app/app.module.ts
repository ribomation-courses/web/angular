import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AppComponent } from './app.component';
import { OperandsComponent } from './operands/operands.component';
import { ResultsComponent } from './results/results.component';
import { EvalService } from './services/eval.service';
import { HttpEvalService } from './services/http-eval.service';
import {HttpModule} from "@angular/http";
import { NotFoundComponent } from './not-found/not-found.component';
import { AdvancedComponent } from './advanced/advanced.component';
import {RouterModule} from "@angular/router";
import {appRoutes} from './app.routes';
import {OperandsService} from "./services/operands.service";

@NgModule({
  declarations: [
    AppComponent,
    OperandsComponent,
    ResultsComponent,
    NotFoundComponent,
    AdvancedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    EvalService,
    HttpEvalService,
    OperandsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
