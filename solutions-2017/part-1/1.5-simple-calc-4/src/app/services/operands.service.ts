import {Injectable} from "@angular/core";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import "rxjs/add/operator/combineAll";
import "rxjs/add/operator/combineLatest";
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import {Result} from "../result";


@Injectable()
export class OperandsService {
  private lhsField: FormControl;
  private rhsField: FormControl;

  setSources(lhs: FormControl, rhs: FormControl): void {
    this.lhsField = lhs;
    this.rhsField = rhs;
  }

  feed(): Observable<Result> {
    if (!this.lhsField || !this.rhsField) {
      throw new Error("[OperandsService] Must invoke setSources() first");
    }

    const lhsFeed$ = this.lhsField
      .valueChanges
      .debounceTime(500)
      //.distinctUntilChanged()
    ;

    const rhsFeed$ = this.rhsField
      .valueChanges
      .debounceTime(500)
      //.distinctUntilChanged()
    ;

    return Observable
      .combineLatest(lhsFeed$, rhsFeed$)
      .map(values => Result.fromOperands(values[0], values[1]))
      ;
  }

}
