/*import {Component, OnInit, Input, OnChanges, SimpleChanges} from "@angular/core";
import {Result} from "../result";
import {EvalService} from "../services/eval.service";
import {HttpEvalService} from "../services/http-eval.service";

@Component({
  selector: "app-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.css"]
})
export class ResultsComponent implements OnInit, OnChanges {
  @Input("lhs") lhs: number = 10;
  @Input("rhs") rhs: number = 12;
  results: Result[] = [];

  constructor(private evalSvc: HttpEvalService) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    this.results = [];

    ['add', 'sub', 'mul', 'div'].forEach(op => {
      this.evalSvc.compute(op, this.lhs, this.rhs, (r: Result) => {
        this.results.push(r);
      });
    });
  }
}*/

import {Component, OnInit} from "@angular/core";
import {Result} from "../result";
import {HttpEvalService} from "../services/http-eval.service";
import {OperandsService} from "../services/operands.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/do";

@Component({
  selector: "app-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.css"]
})
export class ResultsComponent implements OnInit {
  results: Observable<Result[]>;
  loading: boolean = false;

  constructor(private httpEvalSvc: HttpEvalService,
              private opsSvc: OperandsService) {
  }

  ngOnInit() {
    this.results = this.opsSvc.feed()
      .do(() => {
        this.loading = true;
      })
      .switchMap(v => this.httpEvalSvc.computeFeed("all", v.lhs, v.rhs))
      .do(() => {
        this.loading = false;
      })
    ;
  }
}
