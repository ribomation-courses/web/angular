/*
import {Component} from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  lhsApp: number = 30;
  rhsApp: number = 5;
}
*/

import {Component, OnInit} from "@angular/core";
import {NavigationEnd, Router} from "@angular/router";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/map";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  hideRight: boolean = false;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.router.events
      .filter(ev => ev instanceof NavigationEnd)
      .map((ev: NavigationEnd) => ev.urlAfterRedirects)
      .subscribe(uri => this.hideRight = (uri == "/advanced"))
    ;
  }
}
