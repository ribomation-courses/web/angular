import * as express from "express";
import * as cors from "cors";

const port = 3500;
const app = express();
app.use(cors());

app.get("/", (req, res) => {
  res.send({message: "Simple-Calc"});
});

app.get("/add/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send({
    lhs: lhs,
    rhs: rhs,
    operator: "+",
    result: lhs + rhs
  });
});

app.get("/mul/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send({
    lhs: lhs,
    rhs: rhs,
    operator: "*",
    result: lhs * rhs
  });
});

app.get("/sub/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send({
    lhs: lhs,
    rhs: rhs,
    operator: "-",
    result: lhs - rhs
  });
});

app.get("/div/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send({
    lhs: lhs,
    rhs: rhs,
    operator: "/",
    result: lhs / rhs
  });
});

app.get("/all/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send([
    {name: "Addition", lhs: lhs, rhs: rhs, operator: "+", result: lhs + rhs},
    {name: "Subtraction", lhs: lhs, rhs: rhs, operator: "-", result: lhs - rhs},
    {name: "Multiplication", lhs: lhs, rhs: rhs, operator: "*", result: lhs * rhs},
    {name: "Division", lhs: lhs, rhs: rhs, operator: "/", result: lhs / rhs},
  ]);
});


function sum(n: number) {
  return n * (n + 1) / 2;
}

function factorial(n: number) {
  return n <= 1 ? 1 : n * factorial(n - 1);
}

function fibonacci(n: number) {
  return n <= 2 ? 1 : fibonacci(n - 2) + fibonacci(n - 1);
}

app.get("/sum/:arg", (req, res) => {
  const arg = parseInt(req.params.arg);
  const result = sum(arg);
  res.send({
    arg: arg,
    result: result
  });
});

app.get("/fac/:arg", (req, res) => {
  const arg = parseInt(req.params.arg);
  const result = factorial(arg);
  res.send({
    arg: arg,
    result: result
  });
});

app.get("/fib/:arg", (req, res) => {
  const arg = parseInt(req.params.arg);
  const result = fibonacci(arg);
  res.send({
    arg: arg,
    result: result
  });
});

app.get("/adv/:arg", (req, res) => {
  const arg = parseInt(req.params.arg);
  res.send([
    {name: "Sum", operator: "sum", lhs: arg, result: sum(arg)},
    {name: "Factorial", operator: "fac", lhs: arg, result: factorial(arg)},
    {name: "Fibonacci", operator: "fib", lhs: arg, result: fibonacci(arg)}
  ]);
});

app.listen(port, () => {
  console.log("[eval-srv]", "port:", port);
});


