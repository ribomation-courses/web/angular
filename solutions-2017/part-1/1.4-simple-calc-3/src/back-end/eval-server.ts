import * as express from "express";
import * as cors from "cors";

const port = 3500;
const app = express();
app.use(cors());

app.get("/", (req, res) => {
  res.send({message: "Simple-Calc"});
});

app.get("/add/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send({
    lhs: lhs,
    rhs: rhs,
    operator: "+",
    result: lhs + rhs
  });
});

app.get("/mul/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send({
    lhs: lhs,
    rhs: rhs,
    operator: "*",
    result: lhs * rhs
  });
});

app.get("/sub/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send({
    lhs: lhs,
    rhs: rhs,
    operator: "-",
    result: lhs - rhs
  });
});

app.get("/div/:lhs/:rhs", (req, res) => {
  const lhs = parseInt(req.params.lhs);
  const rhs = parseInt(req.params.rhs);
  res.send({
    lhs: lhs,
    rhs: rhs,
    operator: "/",
    result: lhs / rhs
  });
});

app.listen(port, () => {
  console.log("[eval-srv]", "port:", port);
});


