import {Component, OnInit, Input, OnChanges, SimpleChanges} from "@angular/core";
import {Result} from "../result";
import {EvalService} from "../eval/eval.service";
import {HttpEvalService} from "../eval/http-eval.service";

@Component({
  selector: "app-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.css"]
})
export class ResultsComponent implements OnInit, OnChanges {
  @Input("lhs") lhs: number = 10;
  @Input("rhs") rhs: number = 12;
  results: Result[] = [];

  constructor(private evalSvc: HttpEvalService) {
  }

  ngOnInit() {
    //this.results = this.evalSvc.compute(this.lhs, this.rhs);
  }

  ngOnChanges(changes: SimpleChanges): void {
    //this.results = this.evalSvc.compute(this.lhs, this.rhs);
    this.results = [];

    ['add', 'sub', 'mul', 'div'].forEach(op => {
      this.evalSvc.compute(op, this.lhs, this.rhs, (r: Result) => {
        this.results.push(r);
      });
    });

  }
}
