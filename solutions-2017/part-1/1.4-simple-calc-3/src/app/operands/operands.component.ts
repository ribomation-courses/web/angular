import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-operands',
  templateUrl: './operands.component.html',
  styleUrls: ['./operands.component.css']
})
export class OperandsComponent implements OnInit {
  lhs: number = 40;
  rhs: number = 2;
  @Output('left') lhsOut: EventEmitter<number>;
  @Output('right') rhsOut: EventEmitter<number>;

  constructor() {
    this.lhsOut = new EventEmitter<number>();
    this.rhsOut = new EventEmitter<number>();
  }

  compute():void {
    console.info('[operands]', 'compute()', this.lhs, this.rhs);
    this.lhsOut.emit(this.lhs);
    this.rhsOut.emit(this.rhs);
  }

  ngOnInit() {
  }

}
