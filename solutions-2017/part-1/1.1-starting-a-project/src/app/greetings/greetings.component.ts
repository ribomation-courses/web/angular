import {Component, OnInit} from "@angular/core";

@Component({
  selector: "app-greetings",
  template: `
    <p>{{txt}}</p>
    <p>
      {{value}} * 10 = {{mult10(value)}}
    </p>
  `,
  styles: ['p {color:orangered; font-family: sans-serif;}']
})
export class GreetingsComponent implements OnInit {
  txt: string = "tjolla hopp";
  value: number= 12;

  mult10(n: number): number {
    return 10 * n;
  }

  ngOnInit() {
  }
}
