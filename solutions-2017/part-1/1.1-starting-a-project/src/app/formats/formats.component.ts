import {Component} from "@angular/core";

@Component({
  selector: "app-formats",
  templateUrl: "./formats.component.html",
  styleUrls: ["./formats.component.css"]
})
export class FormatsComponent {
  txt = "Foobar strikes againN";
  today = new Date();
  person: any = {
    name:'Anna Conda',
    age: 42
  };

  toggle: boolean = false;
  toggleFormat():void {
    console.log('toggle', this.toggle);
    console.log('fmt', this.fmt);
    this.toggle = !this.toggle;
  }

  get fmt():string {
    return this.toggle ? 'shortDate' : 'yyyy-MM-dd HH:mm:ss';
  }

}
