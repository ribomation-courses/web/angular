import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div style="text-align:center">
      <h1>Welcome to {{title}}!</h1>
      <p>Message: {{msg}}</p>
      <app-greetings></app-greetings>
      <app-formats></app-formats>
    </div>
  `,
  styles: []
})
export class AppComponent {
  title = 'Angular Course';
  name = "jens";
  msg = `hello ${this.name}`;
}
