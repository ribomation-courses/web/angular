export class Result {
  constructor(public name: string,
              public lhs: number,
              public operator: string,
              public rhs: number,
              public result: number) {
  }
}
