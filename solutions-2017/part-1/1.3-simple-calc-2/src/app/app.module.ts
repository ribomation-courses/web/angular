import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import { AppComponent } from './app.component';
import { OperandsComponent } from './operands/operands.component';
import { ResultsComponent } from './results/results.component';
import { EvalService } from './eval/eval.service';

@NgModule({
  declarations: [
    AppComponent,
    OperandsComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [EvalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
