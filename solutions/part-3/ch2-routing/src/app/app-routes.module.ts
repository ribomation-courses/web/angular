import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome/welcome.component';
import {ProductsListComponent} from './products-list/products-list.component';
import {NotFoundComponent} from './not-found.component';
import {ProductViewComponent} from './product-view/product-view.component';

const routes: Routes = [
  {path: 'welcome', component: WelcomeComponent},
  {
    path: 'products', component: ProductsListComponent,
    children: [
      {path: 'detail', component: ProductViewComponent},
    ]
  },
  {path: '', redirectTo: '/welcome', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutesModule {
}
