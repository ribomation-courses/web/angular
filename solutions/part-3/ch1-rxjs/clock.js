const rx = require('rxjs/Rx');

let timer = rx.Observable
  .interval(250)
  .throttleTime(1000)
  .map(_ => new Date())
  .filter(d => Math.floor(d.getTime() / 1000) % 3 !== 0)
  .map(d => d.toISOString().substr(0,19))
  .subscribe(x => console.info(x))
;

// timer
//     .subscribe(_ => console.info('------'))
//     ;

// timer
//     .subscribe(d => console.info('ISO8601 :', d))
//     ;

// timer
//   .map(d => d.replace('T', ' '))
//   .map(d => d.replace(/\.\d+Z$/, ''))
//   .subscribe(d => console.info('DateTime:', d))
//   ;

// timer
//   .map(d => d.replace(/\.\d+Z$/, ''))
//   .map(d => d.substr(11))
//   .subscribe(d => console.info('Time    :', d))
//   ;
