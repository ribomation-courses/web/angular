import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, tap}   from 'rxjs/operators';
import {Profile}    from './profile';

@Injectable()
export class RandomUserProfileService {
  private readonly fields: string = 'name,location,picture,nat,email';
  private readonly uri: string    = 'https://randomuser.me/api/?results=1&format=pretty&noinfo&nat=gb&inc=' + this.fields;

  constructor(private http: HttpClient) {
  }

  /*
   {
   "results": [
   {...},
   {...},
   ],
   "info": {...}
   }
   */
  fetch(): Observable<Profile> {
    return this.http.get(this.uri)
      .pipe(
        tap(data => console.info(data)),
        map((data: any) => data.results),
        map((results: Profile[]) => results[0])
      );
  }

}
