import {Component, OnInit} from '@angular/core';
import {Result}            from './result';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent implements OnInit {
  lhs: number       = 40;
  rhs: number       = 2;
  results: Result[] = [];

  ngOnInit(): void {
    this.compute();
  }

  compute() {
    this.results = [];
    this.results.push(this.add(this.lhs, this.rhs));
    this.results.push(this.sub(this.lhs, this.rhs));
    this.results.push(this.mul(this.lhs, this.rhs));
    this.results.push(this.div(this.lhs, this.rhs));
  }

  private add(left: number, right: number): Result {
    return new Result('addition', '+', left, right, left + right);
  }

  private sub(left: number, right: number): Result {
    return new Result('subtraction', '-', left, right, left - right);
  }

  private mul(left: number, right: number): Result {
    return new Result('multiplication', '*', left, right, left * right);
  }

  private div(left: number, right: number): Result {
    return new Result('division', '/', left, right, left / right);
  }

}
