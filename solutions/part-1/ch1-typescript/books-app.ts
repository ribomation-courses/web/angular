
export class Book {
    constructor(
        private _title:string, 
        private _author:string,
        private _price:number
    ) {}

    get title():string { return this._title.toUpperCase(); }
    get author():string { return this._author.toUpperCase(); }
    get price():number { return 1.25 * this._price; }
}

let ngBook = new Book('Angular Kickstart', 'Anna Conda', 200);

console.log('ng-book: ', `${ngBook.title} by ${ngBook.author}. Price: SEK ${ngBook.price} `);
