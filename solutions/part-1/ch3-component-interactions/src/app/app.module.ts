import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent }      from './app.component';
import {FormsModule}         from '@angular/forms';
import { OperandsComponent } from './operands/operands.component';
import { ResultsComponent }  from './results/results.component';
import {EvalService}         from './eval.service';

@NgModule({
  declarations: [
    AppComponent,
    OperandsComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [EvalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
