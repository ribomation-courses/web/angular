import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Result}                                             from '../result';
import {EvalService}                                        from '../eval.service';

@Component({
  selector:    'app-results',
  templateUrl: './results.component.html',
  styleUrls:   ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnChanges {
  @Input('lhs') lhsIn: number = 20;
  @Input('rhs') rhsIn: number = 30;
  results: Result[] = [];

  constructor(private evalSvc: EvalService) {
    //this.results = evalSvc.compute(3, 5);
  }

  ngOnInit() {
    this.results = this.evalSvc.compute(this.lhsIn, this.rhsIn);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.results = this.evalSvc.compute(this.lhsIn, this.rhsIn);
  }

}
