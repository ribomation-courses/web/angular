import { Injectable } from '@angular/core';
import {Result}       from './result';

@Injectable(/*{
  providedIn: 'root'
}*/)
export class EvalService {

  constructor() { }

  compute(left:number, right:number):Result[] {
    let results = [];
    results.push(this.add(left, right));
    results.push(this.sub(left, right));
    results.push(this.mul(left, right));
    results.push(this.div(left, right));
    return results;
  }

  private add(left: number, right: number): Result {
    return new Result('addition', '+', left, right, left + right);
  }

  private sub(left: number, right: number): Result {
    return new Result('subtraction', '-', left, right, left - right);
  }

  private mul(left: number, right: number): Result {
    return new Result('multiplication', '*', left, right, left * right);
  }

  private div(left: number, right: number): Result {
    return new Result('division', '/', left, right, left / right);
  }

}
