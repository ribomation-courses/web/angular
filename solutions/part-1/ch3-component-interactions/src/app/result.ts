export class Result {
  constructor(
    public name: string,
    public operator: string,
    public lhs: number,
    public rhs: number,
    public result: number,
  ) {
  }
}
