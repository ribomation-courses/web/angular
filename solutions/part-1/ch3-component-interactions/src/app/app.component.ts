import {Component, OnInit} from '@angular/core';
import {Result}            from './result';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  lhsApp: number = 30;
  rhsApp: number = 30;

}
