import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'kr'
})
export class KrPipe implements PipeTransform {
  transform(value: any, decimals: boolean): any {
    const result = (+value).toLocaleString('sv-SE', {
        minimumFractionDigits: decimals ? 2 : 0,
        maximumFractionDigits: decimals ? 2 : 0,
        useGrouping:           true,
      })
      + ' kr';

    const NBSP: string = String.fromCharCode(160);
    return result.replace(/ /g, NBSP);
  }
}
