#!/usr/bin/env bash
set -e

json-server ./db.js --port 3500 --middlewares ./hide-uri.js ./order-decorator.js
