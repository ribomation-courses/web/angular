'use strict';
const YAML = require('yamljs');
const PATH = require('path');
const CRYPTO = require('crypto');

function hash(salt, txt) {
  return CRYPTO.createHmac('sha1', salt).update(txt).digest('hex');
}

module.exports = () => {
  const ymlFile   = PATH.normalize(__dirname + '/../assets/yml/data.yml');
  const imgPrefix = `assets/img/products/`;
  let data        = YAML.load(ymlFile);
  let nextId      = 1;

  data.users.forEach((obj) => {
    obj.id = nextId++;
    obj.password = hash(obj.password, data.misc.salt);
  });

  data.books.forEach((obj) => {
    obj.id = nextId++;
    obj.image = imgPrefix + obj.image;
  });

  data.videos.forEach((obj) => {
    obj.id = nextId++;
    obj.image = imgPrefix + obj.image;
  });

  data.orders = [];

  return data;
};

// ------
//console.log('data', require('./db')());
