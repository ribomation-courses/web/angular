'use strict';

function shouldProcess(uri, op) {
  const prefix = '/orders'
  return uri.startsWith(prefix) && op === 'POST';
}

function processOrder(req, res, nxt) {
  nxt();
}

module.exports = (req, res, nxt) => {
  console.log('[order]', 'url', req.originalUrl, 'op', req.method);

  if (shouldProcess(req.originalUrl, req.method)) {
    processOrder(req, res, nxt);
  } else {
    nxt();
  }
};
