import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-thanks',
  template: `
    <div class="row">
      <div class="col">
        <h1 class="mt-5 text-success text-center">Thank You for your Order</h1>
      </div>
    </div>
  `,
  styles: []
})
export class ThanksComponent implements OnInit {
  constructor(private router: Router) {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.router.navigate(['/home']);
    }, 2000);
  }
}
