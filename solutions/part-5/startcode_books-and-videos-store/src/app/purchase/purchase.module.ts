import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartWidgetComponent } from './cart-widget/cart-widget.component';
import { CheckoutComponent } from './checkout/checkout.component';
import {UtilsModule} from '../utils/utils.module';
import {RouterModule} from '@angular/router';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { OrdersService } from './orders.service';
import {HttpModule} from '@angular/http';
import { ThanksComponent } from './thanks.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    UtilsModule,
  ],
  declarations: [
    CartWidgetComponent,
    CheckoutComponent,
    ShoppingCartComponent,
    ThanksComponent
  ],
  exports: [
    CartWidgetComponent,
    CheckoutComponent
  ],
  providers: [OrdersService]
})
export class PurchaseModule { }
