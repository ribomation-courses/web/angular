import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/empty';

import {environment} from '../../environments/environment';
import {Item} from './shopping-cart.service';

export interface Order {
  id?: string;
  name: string;
  contact: {
    email: string;
    phone?: string;
  };
  address: {
    street: string;
    postCode: string;
    city: string;
  };
  products: Item[];
}


@Injectable()
export class OrdersService {
  private url: string;

  constructor(private http: Http) {
    this.url = environment.backEnd.url;
  }

  saveOrder(order: Order): Observable<Order> {
    return this.http
      .post(`${this.url}/orders`, order)
      .delay(2000)
      .map(res => res.json())
      .do(json => console.log('[orders]', 'recv', json))
      ;
  }

}
