import {Component} from '@angular/core';
import {ShoppingCartService} from '../shopping-cart.service';

@Component({
  selector: 'app-cart-widget',
  templateUrl: './cart-widget.component.html',
  styleUrls: ['./cart-widget.component.css']
})
export class CartWidgetComponent {
  constructor(private _cart: ShoppingCartService) {
  }

  get cart(): ShoppingCartService {
    return this._cart;
  }

}
