import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactComponent} from './site/contact/contact.component';
import {NotFoundComponent} from './site/not-found/not-found.component';
import {HomeComponent} from './site/home/home.component';
import {VideosComponent} from './products/videos/videos.component';
import {BooksComponent} from './products/books/books.component';
import {ProductsModule} from './products/products.module';
import {SiteModule} from './site/site.module';
import {CheckoutComponent} from './purchase/checkout/checkout.component';
import {PurchaseModule} from './purchase/purchase.module';
import {ShoppingCartComponent} from './purchase/shopping-cart/shopping-cart.component';
import {ThanksComponent} from './purchase/thanks.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'books', component: BooksComponent},
  {path: 'videos', component: VideosComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'cart', component: ShoppingCartComponent},
  {path: 'checkout', component: CheckoutComponent},
  {path: 'thanks', component: ThanksComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    SiteModule,
    ProductsModule,
    PurchaseModule,
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutesModule {
}

