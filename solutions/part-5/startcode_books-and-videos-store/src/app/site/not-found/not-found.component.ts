import {Component} from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <div>
      <h1>Ooops, Page Not Found</h1>
      <img src="../../../assets/img/404.png"/>
    </div>
  `,
  styles: ['']
})
export class NotFoundComponent {
}
