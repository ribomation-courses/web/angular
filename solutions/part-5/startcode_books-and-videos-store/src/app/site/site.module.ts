import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NotFoundComponent} from './not-found/not-found.component';
import {ContactComponent} from './contact/contact.component';
import {HomeComponent} from './home/home.component';
import {UtilsModule} from '../utils/utils.module';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    UtilsModule,
  ],
  declarations: [
    NotFoundComponent,
    ContactComponent,
    HomeComponent
  ],
  exports: [
    NotFoundComponent,
    ContactComponent,
    HomeComponent
  ]
})
export class SiteModule {
}
