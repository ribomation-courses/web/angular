Angular
====

Welcome to this course.
The syllabus can be found at
[web/angular-basics](https://www.ribomation.se/courses/web/angular-basics.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code of the demo programs


Installation Instructions
====

In order to do the programming exercises of the course, you need to have
the following installed.

* A modern browser, such as
  - [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
  - [Mozilla FireFox](https://www.mozilla.org/sv-SE/firefox/new/)
  - [MicroSoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)
* A decent IDE, such as
    * JetBrains WebStorm
        - https://www.jetbrains.com/webstorm/download
    * MicroSoft Visual Code
        - https://code.visualstudio.com/
* A BASH terminal, such as the GIT client
  - [GIT Client](https://git-scm.com/downloads)
* NodeJS / NPM
  - https://nodejs.org/en/download/
* Angular CLI
  - `npm install -g @angular/cli`

Usage of this GIT Repo
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

Get the sources initially by a git clone operation

    mkdir -p ~/angular-course/my-solutions
    cd ~/angular-course
    git clone https://gitlab.com/ribomation-courses/web/angular.git gitlab
    

Get the latest updates by a git pull operation

    cd ~/angular-course/gitlab
    git pull

Put your own solutions into `~/angular-course/my-solutions`, one sub-folder per project.


Running the solution and demo apps
====
Before you can run the apps, you need to download the NPM packages for
the project you are interested in. Run the following command inside
the chosen project directory (_N.B. it will take some time..._).

    npm install



***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
