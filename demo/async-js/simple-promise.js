function drawLotteryTicket(numSecs) {
    console.log('drawing the lottery...');
    return new Promise((ok, err) => {
        setTimeout(() => {
            let number = Math.random();
            if (number < 0.75) {
                ok(`++ winner ${number}`);
            } else {
                err('-- looser');
            }
        }, numSecs * 1000);
    });
}

function usePromise() {
    drawLotteryTicket(1)
        .then(console.info, console.error);
}

usePromise();
usePromise();
usePromise();
