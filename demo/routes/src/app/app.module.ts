import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {FirstComponent} from "./first.component";
import {SecondComponent} from "./second.component";
import {ThirdComponent} from "./third.component";
import { FakeDetailsComponent } from './fake-details.component';
import { FakeDataComponent } from './fake-data.component';
import { ProductsComponent } from './products/products.component';
import { ProductOverviewComponent } from './products/product-overview.component';
import { ProductSpecificationComponent } from './products/product-specification.component';
import { LoginComponent } from './login.component';
import { AdminComponent } from './admin.component';
import {AuthService} from "./auth.service";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    FakeDetailsComponent,
    FakeDataComponent,
    ProductsComponent,
    ProductOverviewComponent,
    ProductSpecificationComponent,
    LoginComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
