import {Injectable} from '@angular/core';
import {Product} from '../model/product.model';

@Injectable()
export class ProductDaoService {
  private _products: Product[] = [
    new Product('Angular Primer', 512, 3),
    new Product('Using ng-cli', 128, 5),
    new Product('Angular Testing', 256, 1),
  ];

  constructor() {
  }

  get products(): Product[] {
    return this._products;
  }


}
