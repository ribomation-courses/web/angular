import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import "rxjs/add/operator/map";
import {Result} from "../result";
import {Observable} from "rxjs/Observable";

interface BackendResult {
  lhs: number;
  rhs: number;
  result: number;
  operator: string;
}

@Injectable()
export class HttpEvalService {
  private baseUri: string = "http://localhost:3500";

  constructor(private http: Http) {
  }

  compute(name: string, lhs: number, rhs: number, onResult: (result: Result) => void): void {
    this.http
      .get(`${this.baseUri}/${name}/${lhs}/${rhs}`)
      .map((res: Response) => res.json())
      .subscribe((data: BackendResult) => {
        onResult(new Result(
          name, data.lhs, data.operator, data.rhs, data.result
        ));
      })
    ;
  }

  computeFeed(name: string, lhs: number, rhs?: number): Observable<Result[]> {
    return this.http
      .get(`${this.baseUri}/${name}/${lhs}` + (rhs ? '/' + rhs : ''))
      .map((res: Response) => res.json())
      ;
  }

}
