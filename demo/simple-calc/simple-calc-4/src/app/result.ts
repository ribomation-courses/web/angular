export class Result {
  name: string;
  operator: string;
  lhs: number;
  rhs: number;
  result: number;

  constructor(name: string, lhs: number, operator: string, rhs: number, result: number) {
    this.name = name;
    this.lhs = lhs;
    this.operator = operator;
    this.rhs = rhs;
    this.result = result;
  }

  static fromOperands(lhs: number, rhs: number): Result {
    return new Result(null, lhs, null, rhs, null);
  }

}
