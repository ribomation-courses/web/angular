import {NotFoundComponent} from "./not-found/not-found.component";
import {ResultsComponent} from "./results/results.component";
import {ResultsAdvancedComponent} from "./results-advanced/results-advanced.component";
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";

const appRoutes: Routes = [
  {path: "simple",   component: ResultsComponent},
  {path: "advanced", component: ResultsAdvancedComponent},
  {path: "",         redirectTo: "simple", pathMatch: "full"},
  {path: "**",       component: NotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
