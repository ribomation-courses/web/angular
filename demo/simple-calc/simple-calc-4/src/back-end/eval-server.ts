import * as express from "express";
import * as cors from "cors";

const port = 3500;
const app = express();
app.use(cors());

app.get("/", (req, res) => {
  res.send({message: "Simple-Calc Eval Server"});
});

app.listen(port, () => {
  console.log("[eval-srv]", "port:", port)
});


app.get("/add/:lhs/:rhs", (req, res) => {
  res.send(compute("+", req.params.lhs, req.params.rhs));
});

app.get("/sub/:lhs/:rhs", (req, res) => {
  res.send(compute("-", req.params.lhs, req.params.rhs));
});

app.get("/mul/:lhs/:rhs", (req, res) => {
  res.send(compute("*", req.params.lhs, req.params.rhs));
});

app.get("/div/:lhs/:rhs", (req, res) => {
  res.send(compute("/", req.params.lhs, req.params.rhs));
});

app.get("/all/:lhs/:rhs", (req, res) => {
  res.send(
    ["+", "-", "*", "/"]
      .map(op => compute(op, req.params.lhs, req.params.rhs))
  );
});

// -------
app.get("/sum/:arg", (req, res) => {
  res.send(computeAdvanced("sum", req.params.arg));
});

app.get("/fac/:arg", (req, res) => {
  res.send(computeAdvanced("fac", req.params.arg));
});

app.get("/fib/:arg", (req, res) => {
  res.send(computeAdvanced("fib", req.params.arg));
});

app.get("/adv/:arg", (req, res) => {
  res.send(["sum", "fac", "fib"]
    .map(op => computeAdvanced(op, req.params.arg)));
});


// ---------
app.get("/*/:lhs/:rhs", (req, res) => {
  res.status(404).send({operator: "invalid"});
});


// ----------------------------------------------
function compute(op: string, LHS: string, RHS: string) {
  const lhs = parseInt(LHS);
  const rhs = parseInt(RHS);
  console.log("[eval-srv]", lhs, op, rhs);

  let result: number = 0;
  let name: string = "";
  switch (op) {
    case "+":
      name = "Addition";
      result = lhs + rhs;
      break;
    case "-":
      name = "Subtraction";
      result = lhs - rhs;
      break;
    case "*":
      name = "Multiplication";
      result = lhs * rhs;
      break;
    case "/":
      name = "Division";
      result = lhs / rhs;
      break;
  }

  return {
    name: name,
    operator: op,
    lhs: lhs,
    rhs: rhs,
    result: result
  };
}

function computeAdvanced(op: string, ARG: string) {
  const arg = parseInt(ARG);
  console.log("[eval-srv]", op, arg);

  let result = 0;
  let name: string = "";
  switch (op) {
    case "sum":
      name = "Sum";
      result = sum(arg);
      break;
    case "fac":
      name = "Factorial";
      result = factorial(arg);
      break;
    case "fib":
      name = "Fibonacci";
      result = fibonacci(arg);
      break;
  }
  return {
    name: name,
    operator: op,
    lhs: arg,
    result: result
  };
}


function sum(n: number): number {
  return n * (n + 1) / 2;
}

function factorial(n: number): number {
  return n <= 1 ? 1 : n * factorial(n - 1);
}

function fibonacci(n: number): number {
  return n <= 2 ? 1 : fibonacci(n - 2) + fibonacci(n - 1);
}


