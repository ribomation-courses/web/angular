import * as express from "express";
import * as cors from "cors";

const port = 3500;
const app = express();
app.use(cors());

app.get("/", (req, res) => {
  res.send({message: "Simple-Calc Eval Server"});
});

app.listen(port, () => {
  console.log("[eval-srv]", "port:", port)
});


app.get("/add/:lhs/:rhs", (req, res) => {
  res.send(compute("+", req.params.lhs, req.params.rhs));
});

app.get("/sub/:lhs/:rhs", (req, res) => {
  res.send(compute("-", req.params.lhs, req.params.rhs));
});

app.get("/mul/:lhs/:rhs", (req, res) => {
  res.send(compute("*", req.params.lhs, req.params.rhs));
});

app.get("/div/:lhs/:rhs", (req, res) => {
  res.send(compute("/", req.params.lhs, req.params.rhs));
});

app.get("/*/:lhs/:rhs", (req, res) => {
  res.status(404).send({operator: "invalid"});
});


// ----------------------------------------------
function compute(op: string, LHS: string, RHS: string) {
  const lhs = parseInt(LHS);
  const rhs = parseInt(RHS);
  console.log("[eval-srv]", lhs, op, rhs);

  let result: number = 0;
  switch (op) {
    case "+":
      result = lhs + rhs;
      break;
    case "-":
      result = lhs - rhs;
      break;
    case "*":
      result = lhs * rhs;
      break;
    case "/":
      result = lhs / rhs;
      break;
  }

  return {
    lhs: lhs,
    rhs: rhs,
    operator: op,
    result: result
  };
}
