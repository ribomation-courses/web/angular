import {Component} from "@angular/core";

@Component({
  selector: "app-demo-ngif-2",
  template: `
    <p>
      <button type="button" (click)="toggleName()">Toggle Name</button>
      <span *ngIf="name; then whenTrue; else whenFalse"></span>
      <ng-template #whenTrue>Name: {{name}}</ng-template>
      <ng-template #whenFalse>No name</ng-template>
    </p>
  `,
  styles: []
})
export class DemoNgif2Component {
  name: string = null;
  toggleName(): void {
    this.name = this.name ? null : "Per Silja";
  }
}
