import {Component} from "@angular/core";

@Component({
  selector: "app-demo-ngstyle",
  template: `
    <p id="color" [ngStyle]="{'background-color':color}">The background color is {{color}}</p>
    <p id="size"  [ngStyle]="{'font-size.pt':size}">The font size is {{size}}pt</p>
    <button type="button" (click)="toggleColor()">Toggle Color</button>
    <button type="button" (click)="toggleSize()">Toggle Size</button>
  `,
  styles: [`
    #color {color:white;}
    #size {background-color: lightgray;}
  `]
})
export class DemoNgstyleComponent {
  color: string = "red";
  size: number = 16;
  toggleColor(): void {
    this.color = (this.color == "red") ? "blue" : "red";
  }
  toggleSize(): void {
    this.size = (this.size == 16) ? 42 : 16;
  }
}
