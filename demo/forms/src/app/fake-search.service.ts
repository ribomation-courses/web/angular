import {Injectable} from "@angular/core";

const languages = [
  "JavaScript", "TypeScript", "Java", "C++",
  "C", "C#", "Cobol", "Clojure", "Groovy",
  "Go", "Fortran", "Forth", "Lisp", "Scala",
  "Simula", "SmallTalk",
];

@Injectable()
export class FakeSearchService {
  search(phrase: string): string[] {
    if (!phrase || phrase.length === 0) {return [];}
    const txt = phrase.toLowerCase();
    return languages
      .filter(lang => lang.toLowerCase().includes(txt));
  }
}

