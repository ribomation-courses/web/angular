import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  message: string = 'Foobar Strikes Again';
  today: Date = new Date();
  person: any = {
    name: 'Anna Gram', age: 42, female: true, weight: 52.13
  };
  toggle: boolean = false;
  toggleFormat():void {
    this.toggle = !this.toggle;
  }
  get fmt() :string {
    return this.toggle ? 'full' : 'yyyy-MM-dd HH:mm:ss';
  }
  constructor() {}
  ngOnInit() {}
}
