import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-greetings',
  template: `
    <div>
      <p>Text: {{txt}}</p>
      <p>Value: {{value}} * 10 = {{multiplyBy10(value)}}</p>
    </div>`,
  styles: ['p {color:orange; font-family: sans-serif; font-size: xx-large;}']
})
export class GreetingsComponent implements OnInit {
  txt: string = 'Tjolla Hopp';
  value: number = 42;
  multiplyBy10(num: number) {return num * 10;}
  constructor() {}
  ngOnInit() {}
}
