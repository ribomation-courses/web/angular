import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div>
      <h1>Welcome to {{title}}!</h1>
      <p>Message: {{msg}}</p>
    </div>
    <app-greetings></app-greetings>
    <hr/>
    <app-messages></app-messages>
  `,
  styles: [`
    div {text-align:center;}
  `]
})
export class AppComponent {
  title = 'Angular Course';
  msg = 'Foobar strikes again';
}

