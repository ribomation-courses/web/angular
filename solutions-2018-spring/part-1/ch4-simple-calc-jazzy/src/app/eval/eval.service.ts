import {Injectable} from '@angular/core';
import {Result} from '../model/result';
import {AppComponent} from '../app.component';

@Injectable()
export class EvalService {

  constructor() {
  }

  compute(lhs: number, rhs: number): Result[] {
    let results: Result[] = [];
    results.push(this.add(lhs, rhs));
    results.push(this.sub(lhs, rhs));
    results.push(this.mul(lhs, rhs));
    results.push(this.div(lhs, rhs));
    return results;
  }

  private add(lhs: number, rhs: number): Result {
    return new Result('Addition', lhs, '+', rhs, lhs + rhs);
  }

  private sub(lhs: number, rhs: number): Result {
    return new Result('Subtraction', lhs, '-', rhs, lhs - rhs);
  }

  private mul(lhs: number, rhs: number): Result {
    return new Result('Multiplication', lhs, '*', rhs, lhs * rhs);
  }

  private div(lhs: number, rhs: number): Result {
    return new Result('Division', lhs, '/', rhs, lhs / rhs);
  }

}
