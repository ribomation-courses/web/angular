import {Component, OnInit} from '@angular/core';
import {Result} from './model/result';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  lhs: number = 10;
  rhs: number = 10;

  ngOnInit(): void {

  }
}
