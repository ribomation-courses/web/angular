import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-operands',
  templateUrl: './operands.component.html',
  styleUrls: ['./operands.component.css']
})
export class OperandsComponent implements OnInit {
  lhs: number = 10;
  rhs: number = 10;
  @Output('lhs') lhsOut: EventEmitter<number> = new EventEmitter<number>();
  @Output('rhs') rhsOut: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  compute(): void {
    console.log('[operands]', 'compute()', this.lhs, this.rhs);
    this.lhsOut.emit(this.lhs);
    this.rhsOut.emit(this.rhs);
  }

}
