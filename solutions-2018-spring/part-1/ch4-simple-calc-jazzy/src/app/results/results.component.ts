import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Result} from '../model/result';
import {EvalService} from '../eval/eval.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnChanges {
  @Input('lhs') lhs: number = 30;
  @Input('rhs') rhs: number = 30;
  results: Result[] = [];

  constructor(private evalSvc: EvalService) {
  }

  ngOnInit() {
    this.results = this.evalSvc.compute(this.lhs, this.rhs);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.results = this.evalSvc.compute(this.lhs, this.rhs);
  }

}
