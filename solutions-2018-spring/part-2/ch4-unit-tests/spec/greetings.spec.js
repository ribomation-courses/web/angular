const G = require('../greetings');

describe('simple tests', () => {
  it('should return something', () => {
    let msg = G.greetings('Jens');
    expect(msg).toContain('ens');
  });

  it('should return something at the beginning', () => {
    let msg = G.greetings('Jens');
    expect(msg).toContain('Hello');
  });

});
