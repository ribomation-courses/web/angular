export class Book {
  constructor(
  private  _title: string,
  private  _author: string,
  private  _price: number,
  ) {}

  get title(): string {return this._title.toUpperCase();}
  get author(): string {return this._author.toUpperCase();}
  get price(): number {return 1.25 * this._price;}

  toString(): string {
    return `Book{${this.title}, ${this.author}, ${this.price} kr}`;
  }
}
