
function delay(numSecs) {
  return new Promise((ok,err) => {
    setTimeout(() => {
      ok(numSecs);
    }, numSecs * 1000);
  });
}

console.info('delay...');
delay(1)
  .then((n) => {
    console.info('delay...OK', n, ' secs');
    return delay(4*n);
  })
  .then((x) => {
    console.info('delay...OK', x, ' secs');
  })
  ;
