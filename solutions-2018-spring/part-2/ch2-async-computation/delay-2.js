
function delay(numSecs) {
  return new Promise((ok,err) => {
    setTimeout(() => {
      console.info('timeout', numSecs);
      ok(numSecs);
    }, numSecs * 1000);
  });
}

async function useDelay() {
  let n1 = await delay(1);
  let n2 = await delay(2*n1);
  let n3 = await delay(2*n2);
}

console.info('useDelay...');
useDelay();
