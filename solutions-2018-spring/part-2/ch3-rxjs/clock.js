const rx = require('rxjs/Rx');

let timer = rx.Observable
  .interval(1000)
  .map(_ => new Date())
  .map(d => d.toISOString())
  ;

  timer
    .subscribe(_ => console.info('------'))
    ;

    timer
      .subscribe(d => console.info('ISO8601 :', d))
      ;

timer
  .map(d => d.replace('T', ' '))
  .map(d => d.replace(/\.\d+Z$/, ''))
  .subscribe(d => console.info('DateTime:', d))
  ;

timer
  .map(d => d.replace(/\.\d+Z$/, ''))
  .map(d => d.substr(11))
  .subscribe(d => console.info('Time    :', d))
  ;
