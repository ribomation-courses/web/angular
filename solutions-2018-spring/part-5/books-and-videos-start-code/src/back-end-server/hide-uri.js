'use strict';

function shouldHide(reqUri) {
  const URIs = [
    '/misc', '/users'
  ];
  return URIs.some(uri => reqUri.startsWith(uri));
}

module.exports = (req, res, nxt) => {
  //console.log('[hide-uri]', 'url', req.originalUrl);
  if (shouldHide(req.originalUrl)) {
    res.sendStatus(401);
  } else {
    nxt();
  }
};
