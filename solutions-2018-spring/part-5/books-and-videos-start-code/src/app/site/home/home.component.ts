import {Component} from '@angular/core';
import {NgbSlideEvent} from '@ng-bootstrap/ng-bootstrap/carousel/carousel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  currentSlide: number = 0;
  slider: any = [
    {
      img: 'assets/img/slider/books-1.jpg',
      title: 'Books',
      text: 'Lots of books'
    },
    {
      img: 'assets/img/slider/books-2.jpg',
      title: 'Books to read',
      text: 'Just dig into it'
    },
    {
      img: 'assets/img/slider/books-3.jpg',
      title: 'Books to enjoy',
      text: 'Have fun while reading'
    },
    {
      img: 'assets/img/slider/videos-1.jpg',
      title: 'Videos',
      text: 'Relaxing with a movie'
    },
    {
      img: 'assets/img/slider/videos-2.jpg',
      title: 'Movies',
      text: 'Watching film is fun'
    },
    {
      img: 'assets/img/slider/videos-3.jpg',
      title: 'Films',
      text: 'Having a good time with a video'
    },
  ];

  update(info: NgbSlideEvent): void {
    console.log('[home]', 'slide', info);
    this.currentSlide = +info.current;
  }
}
