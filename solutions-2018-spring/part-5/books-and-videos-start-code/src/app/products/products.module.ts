import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BooksComponent } from './books/books.component';
import { VideosComponent } from './videos/videos.component';
import { DataSourceService } from '../domain/datasource.service';
import {HttpModule} from '@angular/http';
import {UtilsModule} from '../utils/utils.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {PurchaseModule} from '../purchase/purchase.module';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    PurchaseModule,
    UtilsModule,
  ],
  declarations: [BooksComponent, VideosComponent],
  exports: [BooksComponent, VideosComponent],
  providers: [DataSourceService]
})
export class ProductsModule { }
