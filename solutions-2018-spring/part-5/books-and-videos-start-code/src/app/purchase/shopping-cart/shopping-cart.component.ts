import {Component} from '@angular/core';
import {ShoppingCartService} from '../shopping-cart.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent  {

  constructor(public _cart: ShoppingCartService) {
  }

  get cart(): ShoppingCartService {
    return this._cart;
  }

  clearCart(): void {
    this._cart.clear();
  }

  gotoPurchase(): void {

  }

}
