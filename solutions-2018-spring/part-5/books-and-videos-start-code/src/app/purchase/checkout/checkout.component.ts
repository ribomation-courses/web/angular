import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ShoppingCartService} from '../shopping-cart.service';
import {DataSourceService} from '../../domain/datasource.service';
import {Order, OrdersService} from '../orders.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  submitting: boolean = false;
  vatPercentage: number = 0.25;
  shippingFee: number = 95;
  form: FormGroup;

  constructor(private _cart: ShoppingCartService,
              private ws: DataSourceService,
              private orders: OrdersService,
              private router: Router,
              private formBld: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.formBld.group({
      name: ['', Validators.required],
      contact: this.formBld.group({
        email: ['', Validators.email],
        phone: ['',],
      }),
      address: this.formBld.group({
        street: ['', Validators.required],
        postCode: ['', Validators.required],
        city: ['', Validators.required],
      }),
    });

    if (this.cart.count === 0) {
      this.router.navigate(['/home']);
    }

  }

  get cart(): ShoppingCartService {
    return this._cart;
  }

  onSubmit(): void {
    if (this.form.valid) {
      console.log('[checkout]', 'form-data', this.form.value);
      let order: any = this.form.value;
      order.products = this.cart.items;

      this.submitting = true;
      this.orders.saveOrder(order)
        .do(() => this.form.reset())
        .do(() => this.submitting = false)
        .subscribe(savedOrder => {
          console.log('[checkout]', savedOrder);
          this.router.navigate(['/thanks']);
        })
      ;
    }
  }

}
