import {Component} from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <div class="row">
      <div class="col error">
        Ooops, not found.
      </div>
    </div>
  `,
  styles: ['.error {color:red;}']
})
export class NotFoundComponent {
}
