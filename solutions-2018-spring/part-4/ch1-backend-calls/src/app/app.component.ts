import {Component, OnDestroy, OnInit} from '@angular/core';
import {RandomUserProfileService} from './random-user-profile.service';
import {Observable} from 'rxjs/Observable';
import {Profile} from './profile';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  profile$: Observable<Profile>;
  profile: Profile;
  subs: Subscription;

  constructor(private svc: RandomUserProfileService) {
  }

  ngOnInit(): void {
    this.profile$ = this.svc.fetch();
    this.subs = this.profile$.subscribe(p => this.profile = p);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
