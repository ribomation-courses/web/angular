import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'number-stepper',
  templateUrl: './number-stepper.component.html',
  styleUrls: ['./number-stepper.component.css']
})
export class NumberStepperComponent {
  @Input('min') minValue: number | string = 1;
  @Input('max') maxValue: number | string = 100;
  @Input('value') inp: number | string;
  @Output('valueChange') out = new EventEmitter<number>();

  dec() {
    this.resize(-1);
  }

  inc() {
    this.resize(+1);
  }

  resize(delta: number) {
    const nextValue = +this.inp + delta;
    this.inp = Math.min(+this.maxValue, Math.max(+this.minValue, nextValue));
    this.out.emit(this.inp);
  }
}
