import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  obj1: any = {
    name: {
      fname: 'Nisse'
    }
  };
  obj2: any = {};
  fontSize: number = 5;

}
