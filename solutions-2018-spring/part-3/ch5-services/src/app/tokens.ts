import {InjectionToken} from "@angular/core";

export const FACTOR = new InjectionToken<number>('factor');
