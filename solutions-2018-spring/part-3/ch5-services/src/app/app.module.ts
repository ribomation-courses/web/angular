import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {MultiplierService} from './multiplier.service';
import {FACTOR} from './tokens';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    MultiplierService,
    // {provide: MultiplierService, useClass: MultiplierService},
    {provide: FACTOR, useValue: 42}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
