import { TestBed, inject } from '@angular/core/testing';

import { MultiplierService } from './multiplier.service';
import {FACTOR} from './tokens';

describe('MultiplierService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MultiplierService, {provide:FACTOR, useValue:10}]
    });
  });

  it('should be created', inject([MultiplierService], (service: MultiplierService) => {
    expect(service).toBeTruthy();
  }));

  it('when factor is 10 and arg 5, then return 50', inject([MultiplierService], (svc: MultiplierService) => {
    expect(svc.multiply(5)).toBe(50);
  }));
});
