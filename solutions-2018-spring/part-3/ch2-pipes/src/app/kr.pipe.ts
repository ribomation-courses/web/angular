import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'kr'
})
export class KrPipe implements PipeTransform {

  transform(value: any, noDecimals: boolean): any {
    return `${value.toFixed(noDecimals ? 0 : 2).replace('.', ',')} kr`;
  }

}
