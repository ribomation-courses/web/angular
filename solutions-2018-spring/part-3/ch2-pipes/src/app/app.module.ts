import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BoolPipe } from './bool.pipe';
import { KrPipe } from './kr.pipe';


@NgModule({
  declarations: [
    AppComponent,
    BoolPipe,
    KrPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
