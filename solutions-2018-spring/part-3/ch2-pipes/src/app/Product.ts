export class Product {
  name: string = '';
  price: number = 0;
  isService: boolean = false;
  itemsInStore: number = 0;
  lastUpdated: Date = new Date();
}

function randFloat(lb: number, ub: number): number {
  return (lb + (ub - lb + 1) * Math.random());
}

function randInt(lb: number, ub: number): number {
  return Math.floor(randFloat(lb, ub));
}

function randBool() {
  return Math.random() < 0.5;
}

function randDate(numDays: number = 30) {
  const ONE_HOUR = 60 * 60 * 1000;
  const ONE_DAY = 24 * ONE_HOUR;
  return new Date(Date.now() - randInt(1, numDays) * ONE_DAY + randInt(1, 12) * ONE_HOUR);
}

function randElement(arr: any[]) {
  return arr[randInt(0, arr.length - 1)];
}

const names: string[] = [
  'Angular Super Book', 'TypeScript Ultra Book', 'JavaScript for Hackers',
  'NodeJS Ultimate Bible', 'REST Web Services with ExpressJS'
];

export class Generator {
  static mk(): Product {
    let p: Product = new Product();
    p.name         = randElement(names);
    p.itemsInStore = randInt(0, 10);
    p.price        = randFloat(100, 500);
    p.lastUpdated  = randDate(45);
    p.isService    = randBool();
    return p;
  }

  static generate(n: number): Product[] {
    let lst: Product[] = [];
    for (let k = 0; k < n; ++k) lst.push(this.mk());
    return lst;
  }

}
