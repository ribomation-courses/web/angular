import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import 'rxjs/add/operator/map';
// import {map} from 'rxjs/operators'
import {Book, Generator} from '../Product';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent  {
  book: Book;

  constructor(private route: ActivatedRoute) {
    route.params
      .map(p => p.id)
      .map(id => Generator.book(id))
      .subscribe(b => this.book=b)
  }
}
