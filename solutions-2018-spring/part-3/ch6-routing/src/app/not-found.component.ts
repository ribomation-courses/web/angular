import {Component} from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <div class="row">
      <div class="col">
        <h1>Ooops, not found.</h1>
        <img [src]="image404" class="img-fluid rounded"  />
      </div>
    </div>
  `,
  styles: ['h1 {color:red;}']
})
export class NotFoundComponent {
  image404:string = '/assets/404.jpg';
}
