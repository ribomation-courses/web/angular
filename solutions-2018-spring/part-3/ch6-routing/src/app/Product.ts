export class Book {
  id: number;
  name: string;
  pages: number;
  price: number;
  itemsInStore: number;
  lastUpdated: Date;
}

const names: string[] = [
  'Angular Super Book',
  'TypeScript Ultra Book',
  'JavaScript for Hackers',
  'NodeJS Ultimate Bible',
  'REST Web Services with ExpressJS',
  'NPM Kick Start',
  'Up and running with Bower',
  'GulpJS Masters',
  'Twitter Bootstrap Ninja',
  'Mastering FontAwesome'
];

function randInt(lb: number, ub: number): number {
  return Math.floor(lb + (ub - lb + 1) * Math.random());
}

function randElement<T>(arr: T[]): T {
  return arr[randInt(0, arr.length - 1)];
}

function randDate(numDays: number = 30): Date {
  const ONE_DAY: number = 24 * 3600 * 1000;
  return new Date(Date.now() - randInt(1, numDays) * ONE_DAY);
}


export class Generator {
  private static nextBookIndex = 0;
  private static _books: Book[] = [];

  static mk(): Book {
    let p: Book = new Book();
    p.id = this.nextBookIndex++;
    p.name = randElement<string>(names);
    p.pages = randInt(100, 1000);
    p.price = randInt(100, 500);
    p.itemsInStore = randInt(0, 10);
    p.lastUpdated = randDate(45);
    return p;
  }

  static generate(n: number): void {
    this._books = [];
    for (let k = 0; k < n; ++k) this._books.push(this.mk());
  }

  static books(): Book[] {
    if (this._books.length === 0) {
      this.generate(10);
    }
    return this._books;
  }

  static book(id: number): Book {
    if (0 <= id && id < this._books.length) {
      return this._books[id];
    }
    console.warn('book gen', 'invalid id', id);
  }

}
